/********************************************************************************
** Form generated from reading UI file 'boswasi.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOSWASI_H
#define UI_BOSWASI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_boswasi
{
public:
    QWidget *centralWidget;
    QLineEdit *lineEdit;
    QTextBrowser *textBrowser;
    QLineEdit *lineEdit_2;

    void setupUi(QMainWindow *boswasi)
    {
        if (boswasi->objectName().isEmpty())
            boswasi->setObjectName(QStringLiteral("boswasi"));
        boswasi->resize(400, 300);
        centralWidget = new QWidget(boswasi);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(10, 10, 381, 25));
        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(10, 70, 381, 221));
        QFont font;
        font.setPointSize(8);
        textBrowser->setFont(font);
        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(10, 40, 381, 25));
        boswasi->setCentralWidget(centralWidget);

        retranslateUi(boswasi);

        QMetaObject::connectSlotsByName(boswasi);
    } // setupUi

    void retranslateUi(QMainWindow *boswasi)
    {
        boswasi->setWindowTitle(QApplication::translate("boswasi", "boswasi", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class boswasi: public Ui_boswasi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOSWASI_H
