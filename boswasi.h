#ifndef BOSWASI_H
#define BOSWASI_H

#include <QMainWindow>
#include <QApplication>
#include <QDesktopWidget>
#include <QProcess>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <unistd.h>
#include <stdio.h>


namespace Ui {
class boswasi;
}

class boswasi : public QMainWindow
{
    Q_OBJECT

public:
    explicit boswasi(QWidget *parent = 0);
    ~boswasi();

private slots:

    void sre();

    void readsre();

    void stopsre();

    void parse();

    void second_parse();

    void parse_voice();

    void parse_camera();

    void parse_module();

    void parse_status();

    void parse_timer();

    void hello();

    void date_time();

    void setup_math();

    void setup_television();

    void setup_satellite();

    void greet();

    void flitetalk();

    void espeaker();

    void stand_down();

    void system_shutdown();

    void system_reboot();


private:

    bool itsapi;
    bool listen_flag=false;
    bool robvox=false;

    int num_digit=0;
    int num_result;

    QString asrline;
    QString filepath;
    QString homepath;
    QString loadtext;
    QString ttsline;
    QString ttstemp;
    QString voice;

    QStringList vinitials;
    QStringList vnames;

    Ui::boswasi *ui;
};

#endif // BOSWASI_H


