/* this will contain contain Qt code for SRE (speech recognition engine)
* and TTS (text to speech). Will be using a "Qprocess function" to call
* "pocketsphinx_continuous" which is a program dependency. Will make this
* work for RPi and other embedded Linux devices.
* 0.40 add espeak to voice choice
* 0.41 remove talker_flag
* 0.42 capture words but no speaking unless used in context .... in other words no repeat of WHAT
* 0.43 robot voice is default voice using espeak ..espeak is faster than flite
*/

#include "boswasi.h"
#include "ui_boswasi.h"

QObject *parent;

QProcess *asrin = new QProcess (parent);

boswasi::boswasi(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::boswasi)
{
    homepath = QDir::homePath();
    // next lines are for Raspberry
   // if(homepath.contains("pi")==true)
     //   itsapi=true;

    // default voice is stephanie
    voice=homepath+"/flite/voices/cmu_us_slt.flitevox";
    robvox=true;
    QString version = "This is Beyta version 0.43 of Boswasi;... I am in the Stand Down mode right now..... call me when you need something";

    ui->setupUi(this);
    ui->lineEdit->clear();
    ui->lineEdit->insert(version);
   ttstemp=ui->lineEdit->text();

   QFile filein1(homepath+"/boswasi/KEYWORDS");
   filein1.open(QIODevice::ReadOnly | QIODevice::Text);
   QTextStream streamin1(&filein1);
   while(!streamin1.atEnd())
   {
       loadtext = streamin1.readLine();
       ui->textBrowser->append(loadtext);
   }

   ttsline = version;

   vnames<<"ALAN"<<"BRIAN"<<"CHRISTINA"<<"FRED"<<"LAURIE"<<"STEPHANIE";
   vinitials<<"cmu_us_awb"<<"cmu_us_bdl"<<"cmu_us_clb"<<"cmu_us_fem"<<"cmu_us_ljm"<<"cmu_us_slt";

   QFile filein(homepath+"/boswasi/voice.txt");
   filein.open(QIODevice::ReadOnly | QIODevice::Text);

   QTextStream streamin(&filein);
   while(!streamin.atEnd())
   {
       voice=streamin.readLine();
       voice.prepend(homepath+"/flite/voices/");
   }



   greet();

   sre();
}

boswasi::~boswasi()
{
    delete ui;
    asrin->kill();
}

void boswasi::sre()
{



    //set up pocketsphinx_continuous for microphone

    QString prog = "pocketsphinx_continuous";
    QString confmic = "-inmic";
    QString confyes = "yes";
    QString conflm = "-lm";
    QString lm_loc = homepath+"/boswasi/lang/lang.lm";
    QString confdic = "-dict";
    QString dic_loc = homepath+"/boswasi/lang/lang.dic";

    QStringList argu;

    argu<<confmic<<confyes<<conflm<<lm_loc<<confdic<<dic_loc;


    asrin->setEnvironment(QProcess::systemEnvironment());
    asrin->setProcessChannelMode(QProcess::MergedChannels);

    asrin->start(prog,argu);


    connect(asrin,SIGNAL(readyReadStandardOutput()),this,SLOT(readsre()));
    connect(asrin,SIGNAL(finished(int)),this,SLOT(stopsre()));



}

void boswasi::readsre()
{



    int len;

    QString line;
    QByteArray pile;
    QStringList lines;


    QProcess *asrin = dynamic_cast<QProcess *>(sender());


    if(asrin)
    {

        pile=asrin->readAllStandardOutput();
        lines=QString(pile).split("\n");
        foreach (line, lines) {

           if(line.contains("INFO:")|| line.contains(">") || line.contains("-") || line.contains("]"))
           {
                lines.clear();

            }
            else
            {
                asrline=line;
               // asrline =line.append(" ");
                len=line.length();
                asrline.resize(len);

                ui->lineEdit->insert(asrline);

            }
            parse();
        }
        ui->lineEdit->insert(asrin->readAllStandardOutput());
    }


}


void boswasi::stopsre()
{

    asrin->waitForFinished();
    if(asrin->state()!=QProcess::Running)
        asrin->kill();
    parse();
}

void boswasi::parse()
{
    QString line;
    //ui->lineEdit->clear();
    line=asrline;

    if(line.contains("BOSS"))
    {
        line.remove(0,5);
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
        ui->lineEdit->clear();
        asrline=line;
        hello();
    }

    if((line.contains("VOICE")||line.contains("SPEAK"))&& listen_flag==true)
    {
        line.remove(0,6);
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
        ui->lineEdit->clear();
        asrline=line;
        parse_voice();
    }
    if(line.contains("GO")&& listen_flag==true)
    {
       // line.remove(0,5);
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
        ui->lineEdit->clear();
        asrline=line;
        parse_module();
    }

    if(line.contains("SET TIMER")&&listen_flag==true)
    {
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
        ui->lineEdit->clear();
        asrline=line;

    }

    if(line.contains("WHAT")&& listen_flag==true)
    {
        line.remove(0,5);
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
        ui->lineEdit->clear();
        asrline=line;
        second_parse();
    }

       if(line.contains("VIEW")&& listen_flag==true)
    {
        line.remove(0,5);
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
        ui->lineEdit->clear();
        asrline=line;
        parse_camera();

    }

       if(line.contains("SYSTEM")&& listen_flag==true)
       {
           line.remove(0,7);
           ui->lineEdit_2->clear();
           ui->lineEdit_2->insert(line);
           ui->lineEdit->clear();
           asrline=line;
           parse_status();
       }

       if (line.contains("STAND"))
    {
        line.remove(0,6);
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
        ui->lineEdit->clear();
        asrline=line;
        stand_down();

    }

    else return;


}

void boswasi::hello()
{

    QString line = asrline;
    asrline.clear();
    QString voicefile;

    if(line.contains("WAS") && line.contains("HE"))
    {
        line = "I hear you, what do you need?";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);
    }



    ttsline.clear();
    ttsline=line;

    listen_flag=true;

    greet();
}


void boswasi::second_parse()
{
    QString line=asrline;


    if(line.contains("DAY") || line.contains("TIME")|| line.contains("DATE"))
    {
        date_time();
    }

    else{
        ttsline=asrline;
        //greet();

    }

}

void boswasi::parse_voice()
{
    QString line = asrline;
    asrline.clear();
    QString voicefile;
    QString voicename = homepath+"/flite/voices/";
    QString finish = ".flitevox";

    if(line.contains("ALAN")==true)
    {
        robvox=false;
        voicefile="cmu_us_awb"+finish;
        voice=voicename+voicefile;
        line.clear();
        line="Hi this is the Alan voice; I will speak for boswasi.";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);

    }

    else if(line.contains("BRIAN")==true)
    {
        robvox=false;
        voicefile="cmu_us_bdl"+finish;
        voice=voicename+voicefile;
        line.clear();
        line="Hi this is the Brian voice; I will speak for boswasi.";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);


    }

    else if(line.contains("CHRISTINA")==true)
    {
        robvox=false;
        voicefile="cmu_us_clb"+finish;
        voice=voicename+voicefile;
        line.clear();
        line="Hi this is the Christina voice; I will speak for boswasi.";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);

    }

    else if(line.contains("FRED")==true)
    {
        robvox=false;
        voicefile="cmu_us_fem"+finish;
        voice=voicename+voicefile;
        line.clear();
        line="Hi this is the Fred voice; I will speak for boswasi.";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);

    }

    else if(line.contains("LAURIE")==true)
    {
        robvox=false;
        voicefile="cmu_us_ljm"+finish;
        voice=voicename+voicefile;
        line.clear();
        line="Hi this is the Laurie voice; I will speak for boswasi.";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);

    }

    else if(line.contains("STEPHANIE")==true)
    {

        robvox=false;
        voicefile="cmu_us_slt"+finish;
        voice=voicename+voicefile;
        line.clear();
        line="Hi this is the Stephanie voice; I will speak for boswasi.";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);

    }

    else if(line.contains("ROBOT")==true)
    {

        robvox=true;
        line.clear();
        line="Hi this is the ROBOT voice; I will speak for boswasi.";
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);

    }
    else return;

    ttsline.clear();
    ttsline=line;

    QFile file(homepath+"/boswasi/voice.txt");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&file);
    line=out.readLine();
    out<<voicefile;
    file.close();

    greet();

}


void boswasi::parse_camera()
{
    QString no_module="The camera module is not ready yet!";

    QString line = asrline;
    asrline.clear();
  //  QString voicefile;
  //  QString voicename = homepath+"/flite/voices/";
  //  QString finish = ".flitevox";

    if(line.contains("CAMERA")==true)
    {
        line = no_module;
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(line);




    ttsline.clear();
    ttsline=line;
/*
    QFile file(homepath+"/boswasi/voice.txt");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&file);
    line=out.readLine();
    out<<voicefile;
    file.close();
*/
    greet();

   }
   else return;

}

void boswasi::parse_module()
{

    QString line = asrline;
    asrline.clear();

    if(line.contains("MATH")==true)
    {
        setup_math();
    }
    else if(line.contains("TELEVISION")==true)
    {
        setup_television();
     }

    else if(line.contains("SATELLITE")==true)
    {
        setup_satellite();
    }
    else return;

}
void boswasi::parse_status()
{
    QString status_report= "The PC board temperature is  ";
    QString temperature = "23";
    QString temploca = "/sys/class/thermal/thermal_zone0/temp";

    QString line=asrline;
    asrline.clear();
    if(line.contains("STATUS")==true)
    {

    QFile filetempin(temploca);
    if(!filetempin.open(QIODevice::ReadOnly | QIODevice::Text))
    return;

    QTextStream streamintemp(&filetempin);
    {
        loadtext = streamintemp.readLine().trimmed();
        temperature=loadtext;
        temperature.insert(2,QString("."));
    }


    ui->lineEdit_2->clear();
    status_report.append(temperature+" degrees centigrade");

    ui->lineEdit_2->insert(status_report);
    ttsline=status_report;
    greet();
    }

    else if(line.contains("RESTART")==true)
    {
        system_reboot();

    }

    else if(line.contains("POWEROFF")==true)
        system_shutdown();

}

void boswasi::parse_timer()
{
    return;
    QString line=asrline;
    QString timerline = "A timer has been set to ";
    //QString secondline = " second!";
    QString minuteline = " minute!";

    asrline.clear();

    num_digit=0;
       // num_ty=0;
        line = asrline;
        //bool ok;





            line.replace("ZERO",QString::number(0));
            line.replace("ONE",QString::number(1));
            line.replace("TWO",QString::number(2));
            line.replace("THREE",QString::number(3));
            line.replace("FOUR",QString::number(4));
            line.replace("FIVE",QString::number(5));
            line.replace("SIX",QString::number(6));
            line.replace("SEVEN",QString::number(7));
            line.replace("EIGHT",QString::number(8));
            line.replace("NINE",QString::number(9));





               line.replace(" ","");
           //    num_digit = line.toFloat(&ok);
             //  num_result = num_ty + num_digit;
               num_result=num_digit;
               line=QString::number(num_result);
               ui->lineEdit_2->clear();
               ui->lineEdit_2->insert(line);


               //l
               //load_time.clear();
              // load_time=line;
               asrline=line+" has been loaded!";


}

void boswasi::date_time()
{
    QString line=asrline;
    QString dateline = "Today is ";
    QString timeline = "The time is ";

    asrline.clear();

    if(line.contains("DAY")||line.contains("DATE")==true)
    {
        QString cd =QDate::currentDate().toString("dddd MMMM d yyyy");
        dateline.append(cd+".\n");
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(dateline);
        ttsline=dateline;
        greet();
    }

    else if(line.contains("TIME")==true)
    {
        QString ct = QTime::currentTime().toString("h:mm ap");
        timeline.append(ct+".\n");
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(timeline);
        ttsline=timeline;
        greet();
    }
}

void boswasi::setup_math()
{
    QString module = "Leaving boswasi to go to math module.";

    ttsline=module;
    ui->lineEdit_2->clear();
    ui->lineEdit_2->insert(module);

    greet();
    sleep(3);

    QProcess *math_call = new QProcess(this);
    QString prog ="bosmath";

    math_call->startDetached(prog);
    sleep(3);
    QCoreApplication::exit();
}

void boswasi::setup_television()
{
    QString no_module= "The television module isn't set up yet!";

    ttsline=no_module;
    ui->lineEdit_2->clear();
    ui->lineEdit_2->insert(no_module);

    greet();

}

void boswasi::setup_satellite()
{
    QString no_module= "The satellite module isn't set up yet!";

    ttsline=no_module;
    ui->lineEdit_2->clear();
    ui->lineEdit_2->insert(no_module);

    greet();
}

void boswasi::greet()
{

    QString line;

    if(ttsline!=NULL)
    {
    /*
        QFile file(homepath+"/boswasi/flite_file.txt");
        if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);
        line=out.readLine();
        out<<ttsline;
        file.close();
        filepath = homepath+"/boswasi/flite_file_txt";
       // filepath = ui->lineEdit_2->text();

*/
    }
    if(robvox==false)
        flitetalk();
    else if(robvox==true)
        espeaker();
}

void boswasi::flitetalk()
{

    QString prg = "padsp"; // this is a program that allows oss to play flite version 2.0
    QString prog= "flite";
    QString readfile="-t";
    QString voicecall="-voice";
   // QString setstuff="--seti"; //work with integers instead of floats

    QStringList argu;

    argu<<prog<<voicecall<<voice<<readfile<<ttsline;

    QProcess *flt = new QProcess(this);

        flt->start(prg,argu);
        ttsline.clear();



}

void boswasi::espeaker()
{


    QString prog= "espeak";

    QStringList argu;


    argu<<ttsline;

    QProcess *espk = new QProcess(this);

        espk->start(prog,argu);
        ttsline.clear();


        sleep(3);

}


void boswasi::stand_down()
{
    QString line=asrline;
    QString standdown = "Standing down;  just call me when you do need me";


    asrline.clear();

    if(line.contains("DOWN")==true)
    {
        listen_flag=false;
        ui->lineEdit_2->clear();
        ui->lineEdit_2->insert(standdown);
        ttsline=standdown;
        greet();
    }




}

void boswasi::system_reboot()
{
    QString prog= "systemctl";
    QString restart ="reboot";
    QStringList argu;


    argu<<restart;

    QProcess *reboot = new QProcess(this);

    reboot->start(prog,argu);

}

void boswasi::system_shutdown()
{
    QString prog= "systemctl";
    QString restart ="poweroff now";
    QStringList argu;


    argu<<restart;

    QProcess *powerdown = new QProcess(this);

    powerdown->start(prog,argu);

}
